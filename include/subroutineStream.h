#ifndef SUBROUTINESTREAM_H_INCLUDED
#define SUBROUTINESTREAM_H_INCLUDED

class Subroutine{
public:
    void virtual awake() = 0;

    void virtual update() = 0;

    void virtual quit() = 0;

    virtual ~Subroutine(){

    }
};

#endif // SUBROUTINESTREAM_H_INCLUDED
