#include"includeSDL.h"
#include"eventResolution.h"
#include"coreEngine.h"
#include<vector>


#ifndef INPUT_H_INCLUDED
#define INPUT_H_INCLUDED

#define MOUSE_BUTTON_COUNT 5

class Input{
friend class EventResolution;
private:
//Keyboard
    static std::vector<bool> keyDown;
    static std::vector<bool> keyPress;
    static std::vector<bool> keyUp;

    static void setKeyDown(SDL_Scancode cod);

    static void setKeyUp(SDL_Scancode cod);

    static void clearButtonUpdate();


public:
    static bool getKeyUp(SDL_Scancode cod);

    static bool getKey(SDL_Scancode cod);

    static bool getKeyDown(SDL_Scancode cod);

//----
//Mouse
private:
    static Vector2 position;

    static float verticalWheel;

    static float horizontalWheel;

    static std::vector<bool> mouseDown;
    static std::vector<bool> mousePress;
    static std::vector<bool> mouseUp;

    static void setMouseDown(int num);

    static void setMouseUp(int num);

    static void setMousePos(float x, float y);

    static void setMouseWheel(float x, float y);

public:

    static bool getMouseUp(int num);

    static bool getMouseDown(int num);

    static bool getMouse(int num);

    static Vector2 getMousePosition();

    static float getVerticalWheel();

    static float getHorizontalWheel();
//----

};


#endif // INPUT_H_INCLUDED
