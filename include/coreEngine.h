#include<math.h>
#include"string"

#ifndef COREENGINE_H_INCLUDED
#define COREENGINE_H_INCLUDED

#define PI 3.14159265359
#define RADIAN (3.14159265359 / 180.0)
#define DEGREE (180.0 / 3.14159265359)

class Vector2{
public:
    float x;
    float y;

    Vector2(){
        x = 0;
        y = 0;
    }

    Vector2(float x, float y){
        this->x = x;
        this->y = y;
    }

    Vector2(const Vector2& v){
        this->x = v.x;
        this->y = v.y;
    }

    void operator=(const Vector2& v){
    x = v.x;
    y = v.y;
    }

    bool operator==( const Vector2& v) const {
        return (x == v.x && y == v.y);
    }

    bool operator!=( const Vector2& v) const {
        return !(x == v.x && y == v.y);
    }


    bool operator>( const Vector2& v) const {
        return (x > v.x && y > v.y);
    }

    bool operator<( const Vector2& v) const {
        return (x < v.x && y < v.y);
    }

    bool operator>=( const Vector2& v) const {
        return (x >= v.x && y >= v.y);
    }

    bool operator<=( const Vector2& v) const {
        return (x <= v.x && y <= v.y);
    }



    Vector2 operator+( const Vector2& v) const {
        return Vector2(x + v.x, y + v.y);
    }

     Vector2 operator-( const Vector2& v) const {
        return Vector2(x - v.x, y - v.y);
    }

     void operator+=( const Vector2& v) {
        x += v.x;
        y += v.y;
    }

    void operator-=( const Vector2& v) {
        x -= v.x;
        y -= v.y;
    }

    Vector2 operator*(const float& v) const{
        return Vector2(x*v, y *v);
    }

    Vector2 operator/(const float& v) const{
        return Vector2(x/v, y /v);
    }

    void operator*=(const float& v){
        x *= v;
        y *= v;
    }

    void operator/=(const float& v){
        x /= v;
        y /= v;
    }


    float operator*(const Vector2& v){
        return (x * v.x + y * v.y);
    }

    Vector2 perpendecular() const{
        return Vector2(-y,x);

    }

    float magnitude() const{
        return std::sqrt(x*x+y*y);
    }

    float squareMagnitude() const {
        return x*x+y*y;
    }

    void normalise(){
        float l = magnitude();
        if(l > 0){
        this->operator*=((float)1/l);
        }
    }

    Vector2 unit(){
        Vector2 n = *this;
        n.normalise();
        return n;
    }

    void clear(){
        x = y = 0;
    }

    void invert(){
    x = -x;
    y = -y;
    }

    const char* vec_c_str();

    friend std::ostream& operator<<(std::ostream& ostream,const Vector2& vec);
};

class Matrix2{
public:
    float m0,m1,m2,m3;

    Matrix2(){
        m0 = m1 = m2 = m3 = 0;
    }

    Matrix2(const float m0,const float m1,const float m2,const float m3) {
        this->m0 = m0;
        this->m1 = m1;
        this->m2 = m2;
        this->m3 = m3;
    }

    Matrix2(const Matrix2& m) {
        m0 = m.m0;
        m1 = m.m1;
        m2 = m.m2;
        m3 = m.m3;
    }

    void setOrientation(const float& radian) {
        m0 = std::cos(radian); m1 = -std::sin(radian);
        m2 = std::sin(radian); m3 = std::cos(radian);
    }

    void setOrientation(const Vector2& up, const Vector2& right) {
        m0 = right.x; m1 = up.x;
        m2 = right.y; m3 = up.y;
    }

    void transpose() {
        float tmp = m1;
        m1 = m2;
        m2 = tmp;
    }

    void SetSingleMatrix() {
        m0 = 1; m1 = 0;
        m2 = 0; m3 = 1;
    }

    Vector2 transform(const Vector2& v) const {
        return Vector2(
            v.x * m0 + v.y * m1,
            v.x * m2 + v.y * m3
        );
    }

    Vector2 transformTranspose(const Vector2& v) const {
        return Vector2(
            v.x * m0 + v.y * m2,
            v.x * m1 + v.y * m3
        );
    }

    void inverse() {
        float det = m0*m3 - m1*m2;
        if(det != 0){
            Matrix2 tM;
            tM.m0 = m3;
            tM.m3 = m0;
            tM.m1 = -m1;
            tM.m2 = -m2;

            tM *= 1/det;
            *this = tM;
        }
    }

    Vector2 getVector(int index) const {
        if(index == 0){
            return Vector2(m0, m1);
        }else{
            return Vector2(m2, m3);
        }
    }

    Vector2 getVectorTranspose(int index) const {
        if(index == 0){
            return Vector2(m0, m2);
        }else{
            return Vector2(m1, m3);
        }
    }

    float getRadian();

    void operator+=(const Matrix2& m) {
        m0 += m.m0;
        m1 += m.m1;
        m2 += m.m2;
        m3 += m.m3;
    }

    void operator-=(const Matrix2& m) {
        m0 -= m.m0;
        m1 -= m.m1;
        m2 -= m.m2;
        m3 -= m.m3;
    }

    void operator*=(const float& f) {
        m0 *= f;
        m1 *= f;
        m2 *= f;
        m3 *= f;
    }

    void operator/=(const float& f) {
        m0 /= f;
        m1 /= f;
        m2 /= f;
        m3 /= f;
    }

    void operator*=(const Matrix2& m) {
        Matrix2 tmp(m);

        tmp.m0 = m0*m.m0 + m1*m.m2;

        tmp.m1 = m0*m.m1 + m1*m.m3;

        tmp.m2 = m2*m.m0 + m3*m.m2;

        tmp.m3 = m2*m.m1 + m3*m.m3;

        *this = tmp;
    }

    Matrix2 operator+(const Matrix2& m) const {
        return Matrix2(
        m0 + m.m0, m1 + m.m1,
        m2 + m.m2, m3 + m.m3
        );
    }

    Matrix2 operator-(const Matrix2& m) const {
        return Matrix2(
        m0 - m.m0, m1 - m.m1,
        m2 - m.m2, m3 - m.m3
        );
    }

    Matrix2 operator*(const Matrix2& m) const {
        return Matrix2(
        m0*m.m0 + m1*m.m2, m0*m.m1 + m1*m.m3,
        m2*m.m0 + m3*m.m2, m2*m.m1 + m3*m.m3
        );
    }


    Matrix2 operator*(const float& f) const {
        return Matrix2(
        m0 * f, m1 * f,
        m2 * f, m3 * f
        );
    }

    Matrix2 operator/(const float& f) const {
        return Matrix2(
        m0 / f, m1 / f,
        m2 / f, m3 / f
        );
    }

    friend std::ostream& operator<<(std::ostream& ostream,const Matrix2& mat);
};

class RadianMath{
public:
    static float editRadian(float rad){
        float m0 = cos(rad);
        float m1 = -sin(rad);

        float r =acos(m0);
        if(-asin(m1)<0){
            r *= -1;
        }
        return r;
    }
    static float rotateRadian(float curRad, float rotRad){
        Matrix2 m1;
        m1.setOrientation(curRad);
        Matrix2 m2;
        m2.setOrientation(rotRad);
        return (m1 * m2).getRadian();
    }

    static float rotateRadianInverse(float curRad, float rotRad){
        Matrix2 m1;
        m1.setOrientation(curRad);
        m1.inverse();
        Matrix2 m2;
        m2.setOrientation(rotRad);
        return (m1 * m2).getRadian();
    }

};

#endif // COREENGINE_H_INCLUDED
