#include"coreEngine.h"
#include"component.h"
#include"includeSDL.h"
#include"material.h"
#include"subroutineStream.h"
#include"camera.h"
#include<list>
#include<set>

#ifndef RENDERING_STREAM_H_INCLUDED
#define RENDERING_STREAM_H_INCLUDED


struct Bound{
    Vector2 min;
    Vector2 max;

    friend std::ostream& operator<<(std::ostream& ostream,const Bound& mat);

    void rotateBound(const Matrix2& rot);
private:
    static float minValue(const float& a, const float& b, const float& c, const float& d);

    static float maxValue(const float& a, const float& b, const float& c, const float& d);
};

class Render : public Component {
protected:
    Material* material;

    uint drawLayer;

public:
    Render();

    ~Render();

    void setMaterial(Material& material) { this->material = &material; }

    Material* getMaterial() const { return material; }

    void setDrawLayer(uint num);

    uint getDrawLayer() const { return drawLayer; }

    virtual void updateSurface(SDL_Renderer*, const Camera*) const = 0;

    virtual Bound recalculateGlobalBound() const = 0;

    virtual Bound recalculateLocalBound() const = 0;
};


class RenderSDL : public Subroutine {
private: 
    struct RenderCompare {
        bool operator()(const Render* rl, const Render* rr) noexcept { return rl->getDrawLayer() < rr->getDrawLayer(); }
    };

    std::multiset<Render*, RenderCompare> renderObj;

    std::list<Camera*> cameras;
    
public:
    SDL_Renderer* render;
    
    void awake() override;

    void update() override;

    void quit() override;

    bool addRenderComponent(Render* render);

    bool removeRenderComponent(Render* render);

    bool addCamera(Camera* camera);

    bool removeCamera(Camera* camera);

    ~RenderSDL() {}
};

#endif

