#ifndef COMPONENT_H_INCLUDED
#define COMPONENT_H_INCLUDED


class GameObject;

class Component{
    friend GameObject;
protected:
    bool isEnable;
    
    GameObject* gameObject;
public:
    void enable() { if(isEnable) return;  isEnable = true; callEnable(); }
    
    virtual void callEnable() {}

    void disable() { if(!isEnable) return;  isEnable = false; callDisable(); }

    virtual void callDisable() {}

    GameObject* getObject() const { return gameObject; }

    bool getState() { return isEnable; }

    Component() { enable(); }

    virtual ~Component() { disable(); }
};

#endif