#include"subroutineStream.h"

#ifndef SCRIPT_STREAM_H_INCLUDED
#define SCRIPT_STREAM_H_INCLUDED

class ScriptStream : public Subroutine {
public:
    void awake() override;

    void update() override;

    void quit() override;

    ~ScriptStream() {}
};

#endif