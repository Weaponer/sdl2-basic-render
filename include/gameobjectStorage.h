#include"subroutineStream.h"
#include"gameObject.h"
#include<list>

#ifndef GAMEOBJECTSTORAGE_H_INCLUDED
#define GAMEOBJECTSTORAGE_H_INCLUDED



class ObjectStorage : public Subroutine{
private:
    bool isWork;

    std::list<GameObject*> objects;

public:
    void awake() override{
        isWork = true;
    }

    void update() override{

    }

    void quit() override{
        isWork = false;
    }

    GameObject* instantiet();

    bool destroy(GameObject *object);

    ~ObjectStorage(){
    }
};



#endif // GAMEOBJECTSTORAGE_H_INCLUDED
