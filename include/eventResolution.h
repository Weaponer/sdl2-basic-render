#include"includeSDL.h"

#ifndef EVENTRESOLUTION_H_INCLUDED
#define EVENTRESOLUTION_H_INCLUDED

class EventResolution{
private:
   static SDL_Event event;

public:
    static void updateEvent();
};

#endif // EVENTRESOLUTION_H_INCLUDED
