#include"includeSDL.h"

#ifndef TEXTURE_H_INCLUDED
#define TEXTURE_H_INCLUDED

struct Texture{
    SDL_Texture* texture;
};

class TextureManager{
public:
    static Texture* loadTexture(const char* path, SDL_BlendMode mode = SDL_BLENDMODE_ADD);

    static void removeTexutre(Texture* texture);
};

#endif
