#include"renderingStream.h"
#include"coreEngine.h"

#ifndef BASE_RENDER_COMPONENT_H_INCLUDE
#define BASE_RENDER_COMPONENT_H_INCLUDE

class RenderRect : public Render{
protected:
    Vector2 size;
public:
    virtual void updateSurface(SDL_Renderer*, const Camera*) const override;

    virtual Bound recalculateGlobalBound() const override;

    virtual Bound recalculateLocalBound() const override;

    void setSize(const Vector2& vec) { size = vec; }

    Vector2 getSize() const { return size; }
};


class RenderImage : public RenderRect{
public:
    void updateSurface(SDL_Renderer*, const Camera*) const override;

    Bound recalculateGlobalBound() const override;
};
#endif