#include"component.h"

#ifndef CAMERA_H_INCLUDED
#define CAMERA_H_INCLUDED

class Camera : public Component{
private:
    float scale;
    
public:

    Camera();

    ~Camera();
    
    void setScale(float s) { if(s > 0) scale = s; }

    float getScale() const { return scale; }
};

#endif