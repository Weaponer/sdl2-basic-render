
#include"texture.h"

#ifndef MATERIAL_H_INCLUDED
#define MATERIAL_H_INCLUDED

struct Color{
    float r;
    float g;
    float b;
    float a;

    Color(){}

    Color(float r, float g, float b, float a) : r(r), g(g), b(b), a(a) {}

    Color(float r, float g, float b) : r(r), g(g), b(b), a(0) {}
};

class Material{
protected:
    Color baseColor;

    Texture* mainTexture;
public:
    void setMainColor(const Color& color) { baseColor = color; }

    Color getMainColor() { return baseColor; }

    void setMainTexture(Texture* texture) { this->mainTexture = texture; }

    Texture* getMainTexture() const { return mainTexture; }
};

#endif