#include"coreEngine.h"
#include"component.h"
#include<list>
#include<algorithm>
#include<vector>
#include<math.h>
#ifndef GAMEOBJECT_H_INCLUDED
#define GAMEOBJECT_H_INCLUDED

class GameObject;

class Transform{
    friend class GameObject;
private:
    Vector2 position;

    Vector2 localPosition;

    float angleR;

    float localAngleR;

    Transform* parent;
    
    GameObject* gameObject; 

    std::vector<Transform*> listChild;

    Transform(){}

    ~Transform(){}

    void setGameObject(GameObject* gameObject) { this->gameObject = gameObject; }
    
    void editOrientation() const;
public:
    Vector2 getPosition() const { return position; }

    float getAngleR() const { return angleR; }

    Vector2 getLocalPosition() const { return localPosition; }

    float getLocalAngleR() const { return localAngleR; }


    Transform* getParent() const { return parent; }
    
    uint getCountChild() const { return listChild.size(); }

    Transform* getChild(uint num) const { return  listChild[num]; }

    GameObject* getObject() const { return gameObject; }

    void setParent(Transform* Transform);
        
    void setPosition(const Vector2 pos);

    void setLocalPosition(const Vector2 pos);

    void setAngleR(const float radian);

    void setLocalAngleR(const float radian);

    Matrix2 getMatrixRotate() const { Matrix2 m; m.setOrientation(angleR); return m; }

    //static

    /*
    *From local to global cordinate
    */
    static Vector2 transfromVector(const Vector2& vec, const Matrix2& rot, const Vector2& pos);

    /*
    *From global to local cordinate
    */
    static Vector2 transfromInverseVector(const Vector2& vec, const Matrix2& rot, const Vector2& pos);
};

class ObjectStorage;

class GameObject{
    friend class ObjectStorage;
private:
    std::string name;

    Transform transfrom;

    std::list<Component*> components;

    GameObject();

    ~GameObject();

public:
    Transform* getTransform() { return &transfrom; }
    
    std::string getName() const { return name; }

    void setName(const std::string *str) { name = *str; }

   // template<class T> T* GetComponent() const noexcept;
   // template<class T> T* AddComponent() noexcept;

    template<class T> T* getComponent() const {
        for(auto i = components.begin(); i != components.end(); i++){
            if(typeid(T) == typeid(*(*i))){
                return dynamic_cast<T*>(*i);
            }
        }
        return NULL;
    }

    template<class T> T* addComponent() {
        T *com = new T();
        try{
            components.push_back(com);
        }
        catch(const std::exception& e){
            delete com;
            return NULL;
        }
        dynamic_cast<Component*>(com)->gameObject = this;
        return com;
    }

    bool removeComponent(Component* component);
};



#endif // GAMEOBJECT_H_INCLUDED
