#include"includeSDL.h"
#include"renderingStream.h"
#include"gameobjectStorage.h"
#include"scriptStream.h"
#include<queue>
#include<vector>

#ifndef GAMESTREAM_H_INCLUDED
#define GAMESTREAM_H_INCLUDED

#define SCREEN_HEIGHT 800

#define SCREEN_WIDTH 1240



class GameStream{
    static bool quitEve;

public:
    static SDL_Window *window;

    static ObjectStorage objectStorage;

    static RenderSDL render;

    static ScriptStream script;

    static float deltaTime;

    bool static init();

    bool static forceStream();

    void static stopStream();

    void static quit();
};


enum LogType{
    NORMAL_LOG = 0,
    WARNING_LOG = 1,
    ERROR_LOG = 2
};

class Log{
private:
    int type;
    char const* c_str;

public:

    Log(const LogType&,const char*);
    int getType();
    const char* get_c_str();
};


class LogReceiver{
private:
    static std::queue<Log> streamLog;

public:
    static bool logError(const char* c_str);

    static bool logWarning(const char* c_str);

    static bool logNormal(const char* c_str);

    static Log* getLog();
};


#endif // GAMESTREAM_H_INCLUDED
