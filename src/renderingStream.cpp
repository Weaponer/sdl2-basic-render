#include"renderingStream.h"
#include"gameStream.h"
#include"iostream"

Render::Render(){
    drawLayer = 0;
    GameStream::render.addRenderComponent(this);
}

Render::~Render(){
    GameStream::render.removeRenderComponent(this);
}

void Render::setDrawLayer(uint num) {
    GameStream::render.removeRenderComponent(this);
    drawLayer = num;
    GameStream::render.addRenderComponent(this);
}
//Bound

float Bound::minValue(const float& a, const float& b, const float& c, const float& d){
    const float* ar[4] = { &a, &b, &c, &d};
    float min = a;
    for(int i = 0; i < 4; i++)
        if(min >= *ar[i]) min = *ar[i];
    return min;   
}

float Bound::maxValue(const float& a, const float& b, const float& c, const float& d){
    const float* ar[4] = { &a, &b, &c, &d};
    float max = a;
    for(int i = 0; i < 4; i++)
        if(max <= *ar[i]) max = *ar[i];
    return max;
}

void Bound::rotateBound(const Matrix2& rot){
    Vector2 lu{ min.x, max.y };
    Vector2 ld{ min.x, min.y };
    Vector2 ru{ max.x, max.y };
    Vector2 rd{ max.x, min.y };

    lu = rot.transform(lu);
    ld = rot.transform(ld);
    ru = rot.transform(ru);
    rd = rot.transform(rd);
    
    min.x = minValue(lu.x, ld.x, ru.x, rd.x);
    min.y = minValue(lu.y, ld.y, ru.y, rd.y);

    max.x = maxValue(lu.x, ld.x, ru.x, rd.x);
    max.y = maxValue(lu.y, ld.y, ru.y, rd.y);
}

//RenderSDL
void RenderSDL::awake() {
    render = SDL_CreateRenderer(GameStream::window, -1, SDL_RENDERER_ACCELERATED);
    SDL_SetRenderDrawColor(render, 0x00, 0x00, 0x00, 0xFF);
}


static bool getLineContact(const Vector2& p1, const Vector2& p2, const Vector2& m1, const Vector2& m2){
    
    float v = p2.x - p1.x;
    float w = p2.y - p1.y;

    float a = m2.y - m1.y;
    float b = m1.x - m2.x;
    float c = -m1.x * m2.y + m1.y * m2.x;

    float d = (a * v + b * w);

    if(d == 0) {
        return false;
    }

    float t = (-a * p1.x - b * p1.y - c) / d;

    if(t <= 0.0f || t >= 1.0f){
        return false;
    }

    v = m2.x - m1.x;
    w = m2.y - m1.y;

    a = p2.y - p1.y;
    b = p1.x - p2.x;

    c = -p1.x * p2.y + p1.y * p2.x;

    d = (a * v + b * w);

    if(d == 0) {
        return false;
    }

    float t2 = (-a * m1.x - b * m1.y - c) / d;

    if(t2 <= 0.0f || t2 >= 1.0f){
        return false;
    }

    return true;
}

static bool checkBoundCamera(const Matrix2& rC, const Vector2& pC, const Bound& sC, const Vector2& pR, const Bound& sR){
    //stage 1 rendObj on camera

    Vector2 leftUpR(sR.min.x, sR.max.y); leftUpR += pR;
    Vector2 leftDownR(sR.min.x, sR.min.y); leftDownR += pR;
    Vector2 rightUpR(sR.max.x, sR.max.y); rightUpR += pR;
    Vector2 rightDownR(sR.max.x, sR.min.y); rightDownR += pR;

    leftUpR = Transform::transfromInverseVector(leftUpR, rC, pC);
    leftDownR = Transform::transfromInverseVector(leftDownR, rC, pC);
    rightUpR = Transform::transfromInverseVector(rightUpR, rC, pC);
    rightDownR = Transform::transfromInverseVector(rightDownR, rC, pC);
        
    Vector2* arV[4] = { &leftUpR, &rightUpR, &rightDownR , &leftDownR };
    for(int i = 0; i < 4; i++){
        if(sC.max >= (*arV[i]) && sC.min <= (*arV[i])){
            std::cout << 1 << std::endl;
            return true;
        }
    }
    //stage 2 camera on renderObj

    Vector2 up = rC.getVector(1);
    Vector2 right = rC.getVector(0);
    leftUpR = right * sC.min.x + up * sC.max.y + pC;
    leftDownR = right * sC.min.x + up * sC.min.y + pC;
    rightUpR = right * sC.max.x + up * sC.max.y + pC;
    rightDownR = right * sC.max.x + up * sC.min.y + pC;

    Matrix2 zeroM;
    zeroM.setOrientation(0);

    leftUpR = Transform::transfromInverseVector(leftUpR, zeroM, pR);
    leftDownR = Transform::transfromInverseVector(leftDownR, zeroM, pR);
    rightUpR = Transform::transfromInverseVector(rightUpR, zeroM, pR);
    rightDownR = Transform::transfromInverseVector(rightDownR, zeroM, pR);

    for(int i = 0; i < 4; i++){
        if(sR.max >= (*arV[i]) && sR.min <= (*arV[i])){
            std::cout << 2 << std::endl;
            return true;
        }
    } 
    //stage 3 check connect lines

    Vector2 lu(sR.min.x, sR.max.y);
    Vector2 rd(sR.max.x, sR.min.y);
    const Vector2* arVR[4] = { &lu, &(sR.max), &rd, &(sR.min) };

    for(int i = 0; i < 4; i++){
        const Vector2* p1 = arVR[i];
        const Vector2* p2 = arVR[i];
        if(i != 3){
            p2 = arVR[i+1];
        }else{
            p2 = arVR[0];
        }
        for(int i2 = 0; i2 < 4; i2++){
            bool t = false;
            if(i2 != 3){
                t = getLineContact(*p1, *p2, *arV[i2], *arV[i2+1]);
            }else{
                t = getLineContact(*p1, *p2, *arV[i2], *arV[0]);
            }

            if(t){
                std::cout << 3 << std::endl;
                return true;
            }
        }   
    }  
    return false;
}

void RenderSDL::update() {
    SDL_SetRenderDrawColor(render, 0, 0, 0, 0);
    SDL_RenderClear(render);

    Camera* c = nullptr;

    for(auto i = cameras.begin(); i != cameras.end(); ++i){
        if((*i)->getState() == true){
            c = (*i);
        }
    }

    if(c == nullptr) return;

    Matrix2 camMatrix = c->getObject()->getTransform()->getMatrixRotate(); //Preparing camera parameters

    Vector2 posCamera = c->getObject()->getTransform()->getPosition();

    Bound boundCameraArea;
    float y = c->getScale();
    float x = c->getScale() / SCREEN_HEIGHT * SCREEN_WIDTH;
    boundCameraArea.min.x = -x / 2;
    boundCameraArea.min.y = -y / 2;
    boundCameraArea.max.x = x /2;
    boundCameraArea.max.y = y /2;
    //


   for(auto i = renderObj.begin(); i != renderObj.end(); ++i){
        if((*i)->getState() == true){
            Vector2 posObj = (*i)->getObject()->getTransform()->getPosition();
            Bound globSclObj = (*i)->recalculateGlobalBound();
            if(checkBoundCamera(camMatrix, posCamera, boundCameraArea, posObj, globSclObj) == true){
                (*i)->updateSurface(render, c);
            }
        }
    }
    SDL_RenderPresent(render);
}

void RenderSDL::quit() {
    cameras.clear();
    renderObj.clear();
    SDL_DestroyRenderer(render);
}

bool RenderSDL::addCamera(Camera* camera) {
    cameras.push_back(camera);
    return true;
}

bool RenderSDL::removeCamera(Camera* camera) {
    uint l = cameras.size();
    cameras.remove(camera);
    if(l != cameras.size()){
        return true;
    }
    return false;
}


bool RenderSDL::addRenderComponent(Render* rend) {
    renderObj.insert(rend);
    return true;
}

bool RenderSDL::removeRenderComponent(Render* rend) {
    uint l = renderObj.size();
    renderObj.erase(rend);
    if(l != cameras.size()){
        return true;
    }
    return false;
}

std::ostream& operator<<(std::ostream& ostream,const Bound& mat){
    ostream << mat.max << " " << mat.min << " ";
    return ostream;
}