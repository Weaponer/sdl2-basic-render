#include"gameObject.h"
#include<typeinfo>

//Transform 
void Transform::setParent(Transform* transform){
    if(parent != NULL){
            parent->listChild.erase(std::find(listChild.begin(), listChild.end(), transform), listChild.end());
        }
    parent = transform;
    parent->listChild.push_back(this);


    Matrix2 rotP;
    rotP.setOrientation(parent->angleR);
    rotP.inverse();
    localPosition = rotP.transform(position - parent->position);

    Matrix2 myRot;
    myRot.setOrientation(angleR);
    localAngleR = (rotP * myRot).getRadian();
}

void Transform::setPosition(const Vector2 pos){
    position = pos;
    localPosition = pos;
    if(parent){
        Matrix2 rotP;
        rotP.setOrientation(parent->angleR);
        rotP.inverse();
        localPosition = rotP.transform(position - parent->position);
    }
    editOrientation();
}

void Transform::setLocalPosition(const Vector2 pos){
    position = pos;
    localPosition = pos;
    if(parent){
        Matrix2 rotP;
        rotP.setOrientation(parent->angleR);
        position = rotP.transform(localPosition) + parent->position;
    }
    editOrientation();
}

void Transform::setAngleR(const float radian){
    Matrix2 myRot;
    myRot.setOrientation(radian);
    angleR = myRot.getRadian();
    localAngleR = myRot.getRadian();
    if(parent){
        Matrix2 rotP;
        rotP.setOrientation(parent->angleR);
        rotP.inverse();            
        localAngleR = (rotP * myRot).getRadian();
    }
    editOrientation();
}

void Transform::setLocalAngleR(const float radian){
    Matrix2 myRot;
    myRot.setOrientation(radian);
    angleR = myRot.getRadian();
    localAngleR = myRot.getRadian();
    if(parent){
        Matrix2 rotP;
        rotP.setOrientation(parent->angleR);
        rotP.inverse();            
        angleR = (rotP * myRot).getRadian();
    }
    editOrientation();
}

void Transform::editOrientation() const {
    if(listChild.size() > 0){
        for(auto i = listChild.begin(); i != listChild.end(); i++){
            Transform *t = (*i);
            Matrix2 myRot;
            myRot.setOrientation(angleR);
            t->position =  myRot.transform(t->localPosition) + position;

            myRot.inverse();
            Matrix2 baseRot;
            baseRot.setOrientation(t->localAngleR);
            t->angleR = (myRot * baseRot).getRadian();

            t->editOrientation();
        }
    }    
}

//static

Vector2 Transform::transfromVector(const Vector2& vec, const Matrix2& rot, const Vector2& pos){
    Vector2 v = vec;
    v = rot.transform(v);
    v += pos;
    return v;
}


Vector2 Transform::transfromInverseVector(const Vector2& vec, const Matrix2& rot, const Vector2& pos){
    Vector2 v = vec;
    v -= pos;
    Matrix2 m = rot;
    m.inverse();
    return m.transform(v);
}
//GameObject

GameObject::GameObject() {

}

GameObject::~GameObject(){
    for(auto i = components.begin(); i != components.end(); i++){
        delete *i;
    }
    components.clear();
}

bool GameObject::removeComponent(Component* component){
    uint l = components.size();
    components.remove(component);
    if(l != components.size()){
        delete component;
        return true;
    }
    return false;
}