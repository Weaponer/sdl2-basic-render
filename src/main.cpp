#include"coreEngine.h"
#include"gameStream.h"
#include"eventResolution.h"
#include"gameObject.h"
#include"renderingStream.h"
#include"camera.h"
#include"baseRenderComp.h"

int main(){
    if(GameStream::init()){
        do{
            EventResolution::updateEvent();
        } while (GameStream::forceStream());

        GameStream::quit();
        return 0;
    }
    GameStream::quit();
    return 1;
}
