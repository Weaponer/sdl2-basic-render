#include"camera.h"
#include"gameStream.h"


Camera::Camera() {
    GameStream::render.addCamera(this);
}

Camera::~Camera() {
    GameStream::render.removeCamera(this);
}