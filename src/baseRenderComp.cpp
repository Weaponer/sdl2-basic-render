#include"baseRenderComp.h"
#include"gameObject.h"
#include"camera.h"
#include"gameStream.h"
#include<cmath>
#include<iostream>
//Render rect

Bound RenderRect::recalculateLocalBound() const {
    Bound bound;
    bound.min.x = -size.x/2;
    bound.min.y = -size.y/2;

    bound.max.x = size.x/2;
    bound.max.y = size.y/2;
    return bound;
}

Bound RenderRect::recalculateGlobalBound() const { 
    return recalculateLocalBound();
}


void RenderRect::updateSurface(SDL_Renderer* render, const Camera* camera) const {
    Matrix2 camRot;
    camRot.setOrientation(camera->getObject()->getTransform()->getAngleR());
    camRot.inverse();
    Vector2 localPos = Transform::transfromInverseVector(gameObject->getTransform()->getPosition(), camRot, camera->getObject()->getTransform()->getPosition());

    SDL_Rect rect;
    localPos += Vector2(camera->getScale() / SCREEN_HEIGHT * SCREEN_WIDTH / 2.0f, camera->getScale() / 2);

    rect.h = (SCREEN_HEIGHT / camera->getScale()) * size.y;
    rect.w = (SCREEN_WIDTH / (camera->getScale() / SCREEN_HEIGHT * SCREEN_WIDTH)) * size.x;
    rect.x = localPos.x * (SCREEN_WIDTH / (camera->getScale() / SCREEN_HEIGHT * SCREEN_WIDTH)) - (rect.w / 2);
    rect.y = localPos.y * ( SCREEN_HEIGHT / camera->getScale()) - (rect.h / 2);
    

    Color color = material->getMainColor();
    SDL_SetRenderDrawColor(render, color.r, color.g, color.b, color.a);
    SDL_RenderFillRect(render, &rect);
}
//Render image

void RenderImage::updateSurface(SDL_Renderer* render, const Camera* camera) const {
    if(material->getMainTexture() == NULL){
        return;
    }

    Matrix2 camRot;
    camRot.setOrientation(camera->getObject()->getTransform()->getAngleR());
    camRot.inverse();
    Vector2 localPos = Transform::transfromInverseVector(gameObject->getTransform()->getPosition(), camRot, camera->getObject()->getTransform()->getPosition());

    SDL_Rect rect;
    localPos += Vector2(camera->getScale() / SCREEN_HEIGHT * SCREEN_WIDTH / 2.0f, camera->getScale() / 2);

    rect.h = (SCREEN_HEIGHT / camera->getScale()) * size.y;
    rect.w = (SCREEN_WIDTH / (camera->getScale() / SCREEN_HEIGHT * SCREEN_WIDTH)) * size.x;
    rect.x = localPos.x * (SCREEN_WIDTH / (camera->getScale() / SCREEN_HEIGHT * SCREEN_WIDTH)) - (rect.w / 2);
    rect.y = localPos.y * ( SCREEN_HEIGHT / camera->getScale()) - (rect.h / 2);

    float rotate = RadianMath::rotateRadianInverse(gameObject->getTransform()->getAngleR(), camera->getObject()->getTransform()->getAngleR()) * DEGREE;
    SDL_Rect dst;
    dst.w = rect.w;
    dst.h = rect.h;
    dst.x = rect.x;
    dst.y = rect.y;

    SDL_RenderCopyEx(render, material->getMainTexture()->texture, NULL, &dst, rotate, NULL, SDL_FLIP_NONE);
    std::cout << "rend" << std::endl;
}

Bound RenderImage::recalculateGlobalBound() const {
    Bound b = recalculateLocalBound();
    
    Matrix2 rot;
    rot.setOrientation(gameObject->getTransform()->getAngleR());
    b.rotateBound(rot);
    return b;
}