#include"coreEngine.h"
#include<sstream>
#include<iostream>
#include<cmath>

const char* Vector2::vec_c_str(){
    std::ostringstream ss;
    ss << this->x;
    ss << " ";
    ss << this->y;
    return ss.str().c_str();
}

std::ostream& operator<<(std::ostream& ostream,const Vector2& vec){
    ostream << vec.x << " " << vec.y << " ";
    return ostream;
}

std::ostream& operator<<(std::ostream& ostream,const Matrix2& mat){
    ostream << mat.m0 << " " << mat.m1 << std::endl;
    ostream << mat.m2 << " " << mat.m3 << std::endl;
    return ostream;
}

float Matrix2::getRadian() {
    float ang =acos(m0);
    if(-asin(m1)<0){
        ang *= -1;
    }
    return ang;
}