#include"eventResolution.h"
#include"gameStream.h"
#include"input.h"


SDL_Event EventResolution::event;

void EventResolution::updateEvent(){
    Input::clearButtonUpdate();

    while(SDL_PollEvent(&event) != 0){
        if(event.type == SDL_QUIT){
            GameStream::stopStream();
            
        }else if(event.type == SDL_KEYDOWN){
            Input::setKeyDown(event.key.keysym.scancode);

        }else if(event.type == SDL_KEYUP){
            Input::setKeyUp(event.key.keysym.scancode);

        }else if(event.type == SDL_MOUSEMOTION){
            Input::setMousePos(event.motion.x,event.motion.y);

        }else if(event.type == SDL_MOUSEBUTTONDOWN){
            Input::setMouseDown(event.button.button);

        }else if(event.type == SDL_MOUSEBUTTONUP){
            Input::setMouseUp(event.button.button);

        }else if(event.type == SDL_MOUSEWHEEL){
            Input::setMouseWheel(event.wheel.x, event.wheel.y);

        }
    }
}
