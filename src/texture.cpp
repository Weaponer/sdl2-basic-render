#include"texture.h"
#include"gameStream.h"
#include"renderingStream.h"

Texture* TextureManager::loadTexture(const char* path, SDL_BlendMode mode){
    SDL_Surface* loadSurf = IMG_Load(path);
    if(loadSurf != NULL){
        SDL_SetSurfaceBlendMode(loadSurf, mode);
        SDL_Texture* tex = SDL_CreateTextureFromSurface(GameStream::render.render,loadSurf);
        SDL_SetTextureBlendMode(tex, mode);
        SDL_FreeSurface(loadSurf);

        Texture* texture = new Texture();
        texture->texture = tex;
        return texture;
    }else{
        return NULL;
    }
}

void TextureManager::removeTexutre(Texture* texture){
    SDL_DestroyTexture(texture->texture);
    delete texture;
}