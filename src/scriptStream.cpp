#include"scriptStream.h"
#include"coreEngine.h"
#include"gameobjectStorage.h"
#include"gameObject.h"
#include"gameStream.h"
#include"baseRenderComp.h"
#include"texture.h"
#include<iostream>

void ScriptStream::awake() {
    GameObject* cam = GameStream::objectStorage.instantiet();
    Camera* camComp = cam->addComponent<Camera>();

    camComp->setScale(10);
    cam->getTransform()->setPosition(Vector2(0, 0));
    cam->getTransform()->setAngleR(0 * RADIAN);


    GameObject* rect = GameStream::objectStorage.instantiet();
    RenderImage* rectCom = rect->addComponent<RenderImage>();

    rectCom->setSize(Vector2(1.0f,50));

    rect->getTransform()->setPosition(Vector2(7, 0));
    rect->getTransform()->setAngleR(0 * RADIAN);

    Color color(255, 255, 255, 255);
    Material* mat = new Material();
    Texture* texture = TextureManager::loadTexture("./textures/arrow.png");
    mat->setMainTexture(texture);
    mat->setMainColor(color);
    rectCom->setMaterial(*mat);


}

void ScriptStream::update() {

}

void ScriptStream::quit() {

}
