#include"gameStream.h"
#include"gameobjectStorage.h"
#include<unistd.h>
#include<iostream>

//GameStream--------------------------------
float GameStream::deltaTime;

bool GameStream::quitEve;

ObjectStorage GameStream::objectStorage;

RenderSDL GameStream::render;

ScriptStream GameStream::script;

SDL_Window* GameStream::window;

bool GameStream::init(){
    bool isNormal = true;
    if(SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO | SDL_INIT_TIMER | SDL_INIT_EVENTS) < 0){
        isNormal = false;
        LogReceiver::logError(SDL_GetError());
    }
    if(isNormal){
        if(!(IMG_Init(IMG_INIT_JPG | IMG_INIT_PNG | IMG_INIT_TIF | IMG_INIT_WEBP) & (IMG_INIT_JPG | IMG_INIT_PNG | IMG_INIT_TIF | IMG_INIT_WEBP))){
        isNormal = false;
        LogReceiver::logError(IMG_GetError());
        }

        if(TTF_Init()==-1){
            isNormal = false;
            LogReceiver::logError(TTF_GetError());
        }
    }

    if(isNormal){
        window = SDL_CreateWindow("GameEngine", SDL_WINDOWPOS_UNDEFINED,SDL_WINDOWPOS_UNDEFINED,SCREEN_WIDTH,SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
    }

    if(isNormal){//awake game
        objectStorage.awake();
        render.awake();

        script.awake();
    }
    return isNormal;
}

void GameStream::quit(){
    //Quit game
    render.quit();
    objectStorage.quit();    

    script.quit();
    //Destroy SDL
    SDL_DestroyWindow(window);
    SDL_Quit();
    IMG_Quit();
    TTF_Quit();
}

bool GameStream::forceStream(){
    if(quitEve){
        return false;
    }else{
        objectStorage.update();
        render.update();
        
        script.update();
        return true;
    }
}

void GameStream::stopStream(){// SLD Quit event
    quitEve = true;
}


//LogReceiver-------------------------------
std::queue<Log> LogReceiver::streamLog;

bool LogReceiver::logError(const char* c_str){
    Log log(ERROR_LOG,c_str);
    LogReceiver::streamLog.push(log);
    return true;
}

bool LogReceiver::logWarning(const char* c_str){
    Log log(WARNING_LOG,c_str);
    LogReceiver::streamLog.push(log);
    return true;
}

bool LogReceiver::logNormal(const char* c_str){
    Log log(NORMAL_LOG,c_str);
    LogReceiver::streamLog.push(log);
    return true;
}

Log* LogReceiver::getLog(){
    if(!LogReceiver::streamLog.empty()){
        Log* log = &LogReceiver::streamLog.front();
        LogReceiver::streamLog.pop();
        return log;
    }
    return NULL;
}

//Log---------------------------------------
Log::Log(const LogType& t,const char* str){
    this->type = t;
    this->c_str = str;
}

int Log::getType(){
    return this->type;
}

const char* Log::get_c_str(){
    return this->c_str;
}
