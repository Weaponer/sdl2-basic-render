#include"input.h"

//Keyboard
std::vector<bool> Input::keyDown = std::vector<bool>(SDL_NUM_SCANCODES,false);

std::vector<bool> Input::keyPress = std::vector<bool>(SDL_NUM_SCANCODES,false);

std::vector<bool> Input::keyUp = std::vector<bool>(SDL_NUM_SCANCODES,false);


void Input::clearButtonUpdate() {
    std::fill(keyDown.begin(),keyDown.end(),false);
    std::fill(keyUp.begin(),keyUp.end(),false);

    std::fill(mouseDown.begin(),mouseDown.end(),false);
    std::fill(mouseUp.begin(),mouseUp.end(),false);

    verticalWheel = 0;
    horizontalWheel = 0;
}

void Input::setKeyUp(SDL_Scancode cod){
    keyPress[cod] = false;
    keyUp[cod] = true;
}

void Input::setKeyDown(SDL_Scancode cod){
    keyDown[cod] = true;
    keyPress[cod] = true;
}

bool Input::getKey(SDL_Scancode cod){
    return keyPress[cod];
}

bool Input::getKeyUp(SDL_Scancode cod){
    return keyUp[cod];
}

bool Input::getKeyDown(SDL_Scancode cod){
    return keyDown[cod];
}

//Mouse
Vector2 Input::position;

float Input::verticalWheel;

float Input::horizontalWheel;

std::vector<bool> Input::mouseDown = std::vector<bool>(MOUSE_BUTTON_COUNT,false);

std::vector<bool> Input::mouseUp = std::vector<bool>(MOUSE_BUTTON_COUNT,false);

std::vector<bool> Input::mousePress = std::vector<bool>(MOUSE_BUTTON_COUNT,false);

void Input::setMouseUp(int num){
    mouseUp[num] = true;
    mousePress[num] = false;
}

void Input::setMouseDown(int num){
    mouseDown[num] = true;
    mousePress[num] = true;
}

void Input::setMousePos(float x, float y){
    position.x = x;
    position.y = y;
}

Vector2 Input::getMousePosition(){
    return position;
}

bool Input::getMouseUp(int num){
    return mouseUp[num];
}

bool Input::getMouseDown(int num){
    return mouseDown[num];
}

bool Input::getMouse(int num){
    return mousePress[num];
}

void Input::setMouseWheel(float x, float y){
    verticalWheel = y;
    horizontalWheel = x;
}

float Input::getVerticalWheel(){ return verticalWheel; }

float Input::getHorizontalWheel(){ return horizontalWheel; }
